Just a little demo to show how to pass more complicated LabVIEW datatypes back and forth with python.

Return Multiple shows how to return different datatypes by nesting tuples and lists.

Pass Dict shows how to pass a LabVIEW map into Python as a set of keys and values and turn it into a Python Dictionary and how to take a Python Dict and pass out its keys and values and reconstruct it into a LabVIEW Map.

Example Code in LabVIEW 2020
